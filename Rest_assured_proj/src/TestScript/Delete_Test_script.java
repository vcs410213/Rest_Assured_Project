package TestScript;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;

import Common_Methods.API_Trigger;
import Common_Methods.Utilities;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_Test_script  extends API_Trigger {

	public static void execute() throws IOException 
	{
		File logfolder = Utilities.createFolder("Delete_API");
        int statuscode =0;
        for(int i=0;i<5;i++)
        {
		Response response = Delete_API_Trigger(delete_request_body(), delete_endpoint());
         statuscode=response.statusCode();
         statuscode = response.statusCode();
			System.out.println(statuscode);

         if(statuscode==204)
         {
        	 ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("Delete_API_TC5", logfolder, delete_endpoint(), delete_request_body(),
						response.getHeaders().toString(), responseBody.asString());
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 204, "Correct status code not found even after retrying for 5 times");

	}
 
}
