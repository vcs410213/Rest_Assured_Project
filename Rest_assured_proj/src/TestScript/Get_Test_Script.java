package TestScript;
import java.time.LocalDateTime;
import java.util.List;

import org.testng.Assert;
import java.io.IOException;

import Common_Methods.Utilities;
import java.io.File;
import Common_Methods.API_Trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;


public class Get_Test_Script extends API_Trigger {

		public static void execute() throws IOException 
		{
			File logfolder = Utilities.createFolder("Get_API");
	        int statuscode =0;
	        for(int i=0;i<5;i++)
	        {
			Response response = Get_API_Trigger(get_request_body(), get_endpoint());
	         statuscode=response.statusCode();
	         statuscode = response.statusCode();
				System.out.println(statuscode);

	         if(statuscode==200)
	         {
	        	 ResponseBody responseBody = response.getBody();
					System.out.println(responseBody.asString());
					Utilities.createLogFile("Get_API_TC4", logfolder, get_endpoint(), get_request_body(),
							response.getHeaders().toString(), responseBody.asString());
					validate(responseBody);
					break;
				} else {
					System.out.println("Status code found in iteration :" + i + " is :" + statuscode
							+ ", and is not equal expected status code hence retrying");
				}
			}

			Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

		}

	public static void validate(ResponseBody responseBody) {
		
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;

		 String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
	       int[] id={7,8,9,10,11,12};
		    String[] first_name ={"Michael","Lindsay","Tobias","Byron","George","Rachel"};
		    String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
			String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
			

			String exp_url = "https://reqres.in/#support-heading";
			String exp_text= "To keep ReqRes free, contributions towards server costs are appreciated!";

			 int length=responseBody.jsonPath().getInt("data.size()");
		      System.out.println("The length of array is "+length);
				

		      for(int i=0 ; i<length; i++)
				{
				  
				  // String exp_id = Integer.toString(id[i]);
				  // System.out.println(exp_id);
					 int exp_id = id[i];
					 System.out.println(exp_id);
					 
				   String exp_email_id = email[i];
				   System.out.println(exp_email_id);
				   
				   String exp_first_name = first_name[i];
				   System.out.println(exp_first_name);
				   
				   String exp_last_name = last_name[i];
				   System.out.println(exp_last_name);
				   
				   String exp_avatar = avatar[i];
				   System.out.println(exp_avatar);
			
				   //String res_id = jsp_res.getString("data["+i+"].id");
				 int res_id = responseBody.jsonPath().getInt("data["+i+"].id");
				   String res_email =responseBody.jsonPath().getString("data["+i+"].email");
				   String res_first_name = responseBody.jsonPath().getString("data["+i+"].first_name");
				   String res_last_name = responseBody.jsonPath().getString("data["+i+"].last_name");
		 		   String res_avatar = responseBody.jsonPath().getString("data["+i+"].avatar");
				   
		 		  int res_page = responseBody.jsonPath().getInt("page");
					int res_per_page = responseBody.jsonPath().getInt("per_page");
					int res_total = responseBody.jsonPath().getInt("total");
					int res_total_pages = responseBody.jsonPath().getInt("total_pages");
					
					Assert.assertEquals(res_page, exp_page);
					Assert.assertEquals(res_per_page, exp_per_page);
					Assert.assertEquals(res_total, exp_total);
					Assert.assertEquals(res_total_pages, exp_total_pages);


				  
				   Assert.assertEquals(res_id,exp_id);
				   Assert.assertEquals(res_email,exp_email_id);
				   Assert.assertEquals(res_first_name,exp_first_name);
				   Assert.assertEquals(res_last_name,exp_last_name);
				   Assert.assertEquals(res_avatar,exp_avatar);
				// Step 7.3 : Validate support json
					Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
		          Assert.assertEquals(responseBody.jsonPath().getString("support.text"), exp_text, "Validation of text failed");
				}
		}

	
	}




