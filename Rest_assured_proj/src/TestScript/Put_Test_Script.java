package TestScript;
import java.time.LocalDateTime;


import org.testng.Assert;
import java.io.IOException;

import Common_Methods.Utilities;
import java.io.File;
import Common_Methods.API_Trigger;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Put_Test_Script extends API_Trigger {
	
	public static void execute() throws IOException 
	{
		File logfolder = Utilities.createFolder("Put_API");
        int statuscode =0;
        for(int i=0;i<5;i++)
        {
		Response response = Put_API_Trigger(put_request_body(), put_endpoint());
         statuscode=response.statusCode();
         statuscode = response.statusCode();
			System.out.println(statuscode);

         if(statuscode==200)
         {
        	 ResponseBody responseBody = response.getBody();
				System.out.println(responseBody.asString());
				Utilities.createLogFile("Put_API_TC2", logfolder, put_endpoint(), put_request_body(),
						response.getHeaders().toString(), responseBody.asString());
				validate(responseBody);
				break;
			} else {
				System.out.println("Status code found in iteration :" + i + " is :" + statuscode
						+ ", and is not equal expected status code hence retrying");
			}
		}

		Assert.assertEquals(statuscode, 200, "Correct status code not found even after retrying for 5 times");

	}


public static void validate(ResponseBody responseBody) {
	
	String res_name = responseBody.jsonPath().getString("name");
	String res_job = responseBody.jsonPath().getString("job");	
	String res_updatedAt = responseBody.jsonPath().getString("updatedAt");		
	res_updatedAt = res_updatedAt.toString().substring(0, 11);

	// Step 5: Fetch the request body parameters

	JsonPath jsp_req = new JsonPath(put_request_body());
	String req_name = jsp_req.getString("name");
	String req_job = jsp_req.getString("job");

	// Step 6 : Generate expected data.

	LocalDateTime currentdate = LocalDateTime.now();
	String expecteddate = currentdate.toString().substring(0, 11);

	// Step 7 : Validate using TestNG Assertions

	Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
	Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");


} 
}

