package Common_Methods;

import EnviornmentandRepository.RequestRepository;


import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;


public class API_Trigger extends RequestRepository 
      { 
	
	    static String headername = "Content-Type";
		static String headervalue = "application/json";

		public static Response Post_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.post(endPoint);
			return response;
		}
		
		public static Response Put_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.put(endPoint);
			return response;
		}
		
		public static Response Patch_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			req_spec.header(headername, headervalue);
			req_spec.body(requestBody);
			Response response = req_spec.patch(endPoint);
			return response;
		}

		public static Response Get_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			Response response = req_spec.get(endPoint);
			return response;
		}
		
		public static Response Delete_API_Trigger(String requestBody , String endPoint) {

			RequestSpecification req_spec = RestAssured.given();
			Response response = req_spec.delete(endPoint);
			return response;
		}
	}

	

