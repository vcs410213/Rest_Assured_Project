package EnviornmentandRepository;

import java.io.IOException;
import java.util.ArrayList;

import Common_Methods.Utilities;

public class RequestRepository  extends Enviornment
{

	public static String post_param_request_body(String testcasename) throws IOException {
		ArrayList<String> data=Utilities.ReadExcelData("Post_API","TC1");
		String key1=data.get(1);
		String requestname=data.get(2);
		String key2=data.get(3);
		String requestjob=data.get(4);
		String requestBody = "{\r\n" + "    \""+key1+"\": \""+requestname+"\",\r\n" + "    \""+key2+"\": \""+requestjob+"\"\r\n" + "}";
		return requestBody;
	}
	public static String post_request_body() {
		String requestBody ="{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"leader\"\r\n" + "}";
        return requestBody;
	}

	public static String put_request_body() {
		String requestBody ="{\r\n"+ "    \"name\": \"morpheus\",\r\n"+ "    \"job\": \"zion resident\"\r\n"+ "}";  
		return requestBody;
	}

	public static String patch_request_body() {
		String requestBody = "{\r\n" + "    \"name\": \"morpheus\",\r\n" + "    \"job\": \"zion resident\"\r\n" + "}";
		return requestBody;
	}

	public static String get_request_body() {
		String requestBody = "";
		return requestBody;
	}

	
	public static String delete_request_body() {
		String requestBody = "";
		return requestBody;
	}

}
