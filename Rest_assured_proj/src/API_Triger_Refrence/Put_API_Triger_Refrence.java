package API_Triger_Refrence;

import io.restassured.RestAssured;

import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;

import java.time.LocalDateTime;

import org.testng.Assert;

public class Put_API_Triger_Refrence {

	public static void main(String[] args) {
		
		//step 1 : Declare the needed variables
		String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"zion resident\"\r\n"
				+ "}";  
		
		//step 2 : Configure the API 
		
		RestAssured.baseURI = hostname;
		
		String responseBody=given().header(headername,headervalue).body(requestbody).when().put(resource).then().extract().response().asString();
				
				System.out.println(responseBody);
			
			JsonPath jsp_res=new JsonPath(responseBody);
			
			String res_name = jsp_res.getString("name");
			System.out.println(res_name);
			
			String res_job = jsp_res.getString("job");
			System.out.println(res_job);
			
			String res_createdAt = jsp_res.getString("updatedAt");
			res_createdAt = res_createdAt.toString().substring(0, 10);
			System.out.println(res_createdAt);
			
			
			JsonPath jsp_req = new JsonPath(requestbody);
			
			String req_name =jsp_req.getString("name");
			String req_job =jsp_req.getString("job");
			
			LocalDateTime currentdate = LocalDateTime.now();
			String expecteddate = currentdate.toString().substring(0,10);
			
			
			Assert.assertEquals(res_name, req_name);
			Assert.assertEquals(res_job, req_job);
			Assert.assertEquals(res_createdAt, expecteddate);
			
			
			
			
			
			

	}

}
