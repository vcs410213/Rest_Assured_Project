package API_Triger_Refrence;

import io.restassured.RestAssured;




import static io.restassured.RestAssured.given;
import java.time.LocalDateTime;
import org.testng.Assert;
import io.restassured.path.json.JsonPath;

public class Post_API_Triger_Refrence {

	public static void main(String[] args) {
		
      // step 1 : Declare the needed variables and save into local variable
		String hostname = "https://reqres.in/";
		String resource = "/api/users";
		String headername = "Content-Type";
		String headervalue = "application/json";
		String requestbody = "{\r\n"
				+ "    \"name\": \"morpheus\",\r\n"
				+ "    \"job\": \"leader\"\r\n"
				+ "}";  
		
        
		RestAssured.baseURI = hostname;
		
		//Step 3: Configure the API for execution and log entire transaction(request header,request body,response header,response body,time etc) 
		
		//given().log().all().when().get(resource).then().log().all().extract().response().asString(); 
		 
		//Step 4: Configure the API for execution and save the response in String variable
		
		String responseBody=given().header(headername,headervalue).body(requestbody).when().post(resource).then().extract().response().asString();
				
		System.out.println(responseBody);
		
		//Step 5: Parse the response body
		//Step5.1: Create the object of JsonPath
		
		JsonPath jsp_res=new JsonPath(responseBody);
		
		//Step 5.2: Parse individual parameters using jsp_res object
		
		String res_name = jsp_res.getString("name");
		System.out.println(res_name);
		
		String res_job = jsp_res.getString("job");
		System.out.println(res_job);
		
		String res_id = jsp_res.getString("id");
		System.out.println(res_id);
		
		String res_createdAt = jsp_res.getString("createdAt");
	    res_createdAt = res_createdAt.substring(0,10);
		System.out.println(res_createdAt);
		
		//Step 6:Validate the response body
		
		 
		//Step 6.1: Parse the request body and save into variable
         JsonPath jsp_req=new JsonPath(requestbody);
		
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");
		
		//Step 6.2:Generate expected date
		
		LocalDateTime currentdate=LocalDateTime.now();
		String expecteddate=currentdate.toString().substring(0,10);
		
		//Step 6.3 : Use TestNG's assert
		
		Assert.assertEquals(res_name,req_name);
		Assert.assertEquals(res_job,req_job);
		Assert.assertNotNull(res_id);
		Assert.assertEquals(res_createdAt,expecteddate);
		
		
		
		
		
	}

}
