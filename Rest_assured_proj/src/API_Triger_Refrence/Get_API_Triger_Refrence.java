package API_Triger_Refrence;

import static io.restassured.RestAssured.given;




//import java.time.LocalDateTime;


import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Get_API_Triger_Refrence {

	public static void main(String[] args) {
		
		
		 // step 1 : Declare the needed variables and save into local variable
		String hostname = "https://reqres.in/";
		String resource = "/api/users?page=2";
		
		//Declare the expected result
        String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
        int[] id={7,8,9,10,11,12};
	    String[] first_name ={"Michael","Lindsay","Tobias","Byron","George","Rachel"};
	    String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
		
		 
		//Step 2: Declare baseURI
		
		RestAssured.baseURI = hostname;
		
		//Step 3: Configure the API for execution and save the response in String variable
		
		String responsebody = given().when().get(resource).then().extract().response().asString();
		System.out.println(responsebody);
		
			
			
		
		//Step 4: Parse the response body
		//Step 4.1: Create the object of JsonPath
		
		JsonPath jsp_res=new JsonPath(responsebody);
		
		 int length=jsp_res.getInt("data.size()");
	      System.out.println("The length of array is "+length);
			
		
 		//Step 4.2: Parse individual parameters using jsp_res object
		
		
		
		for(int i=0 ; i<length; i++)
		{
		  
		  // String exp_id = Integer.toString(id[i]);
		  // System.out.println(exp_id);
			 int exp_id = id[i];
			 System.out.println(exp_id);
			 
		   String exp_email_id = email[i];
		   System.out.println(exp_email_id);
		   
		   String exp_first_name = first_name[i];
		   System.out.println(exp_first_name);
		   
		   String exp_last_name = last_name[i];
		   System.out.println(exp_last_name);
		   
		   String exp_avatar = avatar[i];
		   System.out.println(exp_avatar);
	
		   //String res_id = jsp_res.getString("data["+i+"].id");
		 int res_id = jsp_res.getInt("data["+i+"].id");
		   String res_email = jsp_res.getString("data["+i+"].email");
		   String res_first_name = jsp_res.getString("data["+i+"].first_name");
		   String res_last_name = jsp_res.getString("data["+i+"].last_name");
 		   String res_avatar = jsp_res.getString("data["+i+"].avatar");
		   
		   
		  
		   Assert.assertEquals(res_id,exp_id);
		   Assert.assertEquals(res_email,exp_email_id);
		   Assert.assertEquals(res_first_name,exp_first_name);
		   Assert.assertEquals(res_last_name,exp_last_name);
		   Assert.assertEquals(res_avatar,exp_avatar);
			
		
		}
		

		
		
		
		
		
		
		
		

		
	}

	

}
