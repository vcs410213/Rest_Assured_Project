package API_Triger_Refrence;

import org.testng.Assert;

import io.restassured.path.json.JsonPath;
import static org.testng.Assert.ARRAY_MISMATCH_TEMPLATE;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.specification.RequestSpecification;

public class Get_Req_Specification_Res1 {

	public static void main(String[] args) {
		
		
		
		 // step 1 : Declare the needed variables and save into local variable
		String hostname = "https://reqres.in";
		String resource = "/api/users?page=2";
		
		//Declare the expected result
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_pages = 2;
		
       String[] email = {"michael.lawson@reqres.in","lindsay.ferguson@reqres.in","tobias.funke@reqres.in","byron.fields@reqres.in","george.edwards@reqres.in","rachel.howell@reqres.in"};
       int[] id={7,8,9,10,11,12};
	    String[] first_name ={"Michael","Lindsay","Tobias","Byron","George","Rachel"};
	    String[] last_name = {"Lawson","Ferguson","Funke","Fields","Edwards","Howell"};
		String[] avatar = {"https://reqres.in/img/faces/7-image.jpg","https://reqres.in/img/faces/8-image.jpg","https://reqres.in/img/faces/9-image.jpg","https://reqres.in/img/faces/10-image.jpg","https://reqres.in/img/faces/11-image.jpg","https://reqres.in/img/faces/12-image.jpg"};
		

		// Step 1.3 : Declare expected results of support

		String exp_url = "https://reqres.in/#support-heading";
		String exp_text= "To keep ReqRes free, contributions towards server costs are appreciated!";

		
		// Step 3: Trigger the API

		// Step 4.1 : Build the request specification using RequestSpecification
		RequestSpecification req_spec = RestAssured.given();

		// Step 4.2 : Trigger the API
		Response response = req_spec.get(hostname + resource);

		// Step 5: Extract the status code
		int statuscode = response.statusCode();
		System.out.println(statuscode);

		// Step 6: Fetch the response body parameters
		ResponseBody responseBody = response.getBody();

		 int length=responseBody.jsonPath().getInt("data.size()");
	      System.out.println("The length of array is "+length);
			

	      for(int i=0 ; i<length; i++)
			{
			  
			  // String exp_id = Integer.toString(id[i]);
			  // System.out.println(exp_id);
				 int exp_id = id[i];
				 System.out.println(exp_id);
				 
			   String exp_email_id = email[i];
			   System.out.println(exp_email_id);
			   
			   String exp_first_name = first_name[i];
			   System.out.println(exp_first_name);
			   
			   String exp_last_name = last_name[i];
			   System.out.println(exp_last_name);
			   
			   String exp_avatar = avatar[i];
			   System.out.println(exp_avatar);
		
			   //String res_id = jsp_res.getString("data["+i+"].id");
			 int res_id = responseBody.jsonPath().getInt("data["+i+"].id");
			   String res_email =responseBody.jsonPath().getString("data["+i+"].email");
			   String res_first_name = responseBody.jsonPath().getString("data["+i+"].first_name");
			   String res_last_name = responseBody.jsonPath().getString("data["+i+"].last_name");
	 		   String res_avatar = responseBody.jsonPath().getString("data["+i+"].avatar");
			   
	 		  int res_page = responseBody.jsonPath().getInt("page");
				int res_per_page = responseBody.jsonPath().getInt("per_page");
				int res_total = responseBody.jsonPath().getInt("total");
				int res_total_pages = responseBody.jsonPath().getInt("total_pages");
				
				Assert.assertEquals(res_page, exp_page);
				Assert.assertEquals(res_per_page, exp_per_page);
				Assert.assertEquals(res_total, exp_total);
				Assert.assertEquals(res_total_pages, exp_total_pages);


			  
			   Assert.assertEquals(res_id,exp_id);
			   Assert.assertEquals(res_email,exp_email_id);
			   Assert.assertEquals(res_first_name,exp_first_name);
			   Assert.assertEquals(res_last_name,exp_last_name);
			   Assert.assertEquals(res_avatar,exp_avatar);
			// Step 7.3 : Validate support json
				Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");
	          Assert.assertEquals(responseBody.jsonPath().getString("support.text"), exp_text, "Validation of text failed");
			}
	}
}
