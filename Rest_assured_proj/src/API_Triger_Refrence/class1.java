package API_Triger_Refrence;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;
import java.time.LocalDateTime;
import org.testng.Assert;

import static io.restassured.RestAssured.given;
public class class1 {

	public static void main(String[] args) {
	
		       String hostname ="https://reqres.in/";
				String resource ="/api/users";
				String headername ="content-type";
				String headervalue = "application/json";
				String requestbody ="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"leader\"\r\n"
						+ "}";
				
				RestAssured.baseURI =hostname;
				//given().log().all().header(headername,headervalue).body(req_body).when().patch(resource).then().log().all().extract().response().asString();	
				
				 String responsebody = given().header(headername,headervalue).body(requestbody).when().post(resource).then().extract().response().asString();
        System.out.println(responsebody);
        
        //Parse the response body
         // create the object of json of responsebody
        
        JsonPath jsp = new JsonPath(responsebody);
         
        String res_name=jsp.getString("name");
        String res_job =jsp.getString("job");
        String res_id =jsp.getString("id");
        String res_createdAt=jsp.getString("createdAt");
        res_createdAt=res_createdAt.substring(0,10);
        
        //parse the req body
        //create the obj of jsonpath
        JsonPath jsp1=new JsonPath(requestbody);
        
        String req_name=jsp1.getString("name");
        String req_job =jsp1.getString("job");
        
        //Generate the expected date
         
        LocalDateTime date= LocalDateTime.now();
        String date1=date.toString().substring(0,10);
        
        //validate
        Assert.assertEquals(res_name,req_name);
        Assert.assertEquals(res_job,req_job);
        Assert.assertNotNull(res_id);
        Assert.assertEquals(res_createdAt,date1);
        
        
        
        
	}

}
