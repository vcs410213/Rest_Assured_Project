Endpoint is :
https://reqres.in/api/users/2

Request body is :
{
    "name": "morpheus",
    "job": "zion resident"
}

Response header is :
Date=Thu, 02 May 2024 08:32:41 GMT
Content-Type=application/json; charset=utf-8
Transfer-Encoding=chunked
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1714638761&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=12lal3HDQeoyit2QkIn6itbnafm9P%2F40EJwUFpX8LOA%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1714638761&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=12lal3HDQeoyit2QkIn6itbnafm9P%2F40EJwUFpX8LOA%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-dWJl8LCSA3j8LAl9c/mlONAKw1I"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=87d6a9805de25f66-SIN
Content-Encoding=gzip

Response body is :
{"name":"morpheus","job":"zion resident","updatedAt":"2024-05-02T08:32:41.390Z"}